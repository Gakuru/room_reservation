const connection = require('../db/connect');

const uuid = require('uuid/v1');

const urls = require('../utils/urls');

const pager = require('../utils/pager');
const transformer = require('../utils/transformer');

const validate = require('../utils/dataValidator');

const errorHandler = require('../utils/error');

const returnPagedRooms = (result, nextStart, totalResultLength) => {
    return {
        rooms: transformer.transformRoom(result),
        pager: pager.getPager(result.length, totalResultLength, pager.options.size, nextStart, urls.courseUrl)
    };
};

module.exports = {
    get: (args) => {
        if (args.id) {
            connection.query('CALL `get_room` (?);', [args.id], (err, results, fields) => {
                if (err) 
                    throw err;
                
                args.callback(transformer.transformRoom(results[0]));

            })
        } else {
            args.page = args.page
                ? (args.page < 0)
                    ? 1
                    : args.page
                : 1;

            pager.options.offSet = ((args.page * pager.options.size) - pager.options.size);

            connection.query('CALL `get_rooms` (?,?);', [
                pager.options.offSet,
                (pager.options.size + 1)
            ], (err, results, fields) => {
                if (err) 
                    throw err;
                
                let totalResultLength = results[0].length;
                let resultSet = ((totalResultLength > pager.options.size)
                    ? results[0].splice(0, pager.options.size)
                    : results[0]);

                args.callback(returnPagedRooms(resultSet, args.page, totalResultLength))

            })
        }
    },
    save: (args) => {

        const {data, callback} = args;

        let response = undefined;

        let dataValidator = validate.validatePost(data, 'room');

        if (dataValidator.isValid) {

            data.id = uuid();

            connection.query('INSERT INTO `room` SET ?;', data , (err, results, fields) => {
                if (err) {

                    response = {
                        action: 'Create room',
                        status: 'failed',
                        code: 400,
                        success: false,
                        err: errorHandler.throwSQLErr(err)
                    }

                    callback(response);

                } else {
                    if (results) {
                        data.link = `${urls.roomUrl}/${data.id}`
                        response = {
                            action: 'Create room',
                            status: 'ok',
                            code: 200,
                            success: true,
                            message: 'room created',
                            results: {
                                room: data
                            }
                        }
                    }
                    callback(response);
                }

                });
            } else {
                response = {
                    action: 'create room',
                    status: 'failed',
                    code: 400,
                    success: false,
                    err: {
                        message: dataValidator.message,
                        resoulution: dataValidator.resolution,
                        err: dataValidator.err
                    }
                }

                callback(response);
        }
    }, update: (args) => {

        const { data, callback } = args;

        let response = undefined;

        let dataValidator = validate.validatePost(data, 'room');

        if (dataValidator.isValid) {

            data.id = uuid();

            connection.query('UPDATE room SET ?  WHERE ?;', data, (err, result, fields) => {
                if (err) {

                    response = {
                        action: 'Update room',
                        status: 'failed',
                        code: 400,
                        success: false,
                        err: errorHandler.throwSQLErr(err)
                    }

                    callback(response);

                } else {
                    if (results) {
                        data.link = `${urls.roomUrl}/${data.id}`
                        response = {
                            action: 'Update room',
                            status: 'ok',
                            code: 200,
                            success: true,
                            message: 'room updated',
                            results: {
                                room: data
                            }
                        }
                    }
                    callback(response);
                }

                });
            } else {
                response = {
                    action: 'update room',
                    status: 'failed',
                    code: 400,
                    success: false,
                    err: {
                        message: dataValidator.message,
                        resoulution: dataValidator.resolution,
                        err: dataValidator.err
                    }
                }

                callback(response);
        }
    }
}