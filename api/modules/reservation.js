const connection = require('../db/connect');

const uuid = require('uuid/v1');

const urls = require('../utils/urls');

const pager = require('../utils/pager');
const transformer = require('../utils/transformer');

const validate = require('../utils/dataValidator');

const errorHandler = require('../utils/error');

const randomstring = require('randomstring');

const returnPagedResrvations = (result, nextStart, totalResultLength) => {
    return {
        reservations: transformer.transformReservations(result),
        pager: pager.getPager(result.length, totalResultLength, pager.options.size, nextStart, urls.courseUrl)
    };
};

const bookingTypes = {
    reservation: {
      name: 'reservation',
      prefix: 'RSVP'
    }
};

const constructRefNo = (bookingType) => {
    return bookingTypes[bookingType].prefix + '-' + randomstring.generate({
      length: 7,
      charset: 'alphanumeric',
      capitalization: 'uppercase'
    });
  };

module.exports = {
    get: (args) => {
        if (args.id) {
            connection.query('CALL `get_reservation` (?);', [args.id], (err, results, fields) => {
                if (err) 
                    throw err;
                
                args.callback(transformer.transformReservations(results[0]));

            })
        } else {
            args.page = args.page
                ? (args.page < 0)
                    ? 1
                    : args.page
                : 1;

            pager.options.offSet = ((args.page * pager.options.size) - pager.options.size);

            connection.query('CALL `get_reservations` (?,?);', [
                pager.options.offSet,
                (pager.options.size + 1)
            ], (err, results, fields) => {
                if (err) 
                    throw err;
                
                let totalResultLength = results[0].length;
                let resultSet = ((totalResultLength > pager.options.size)
                    ? results[0].splice(0, pager.options.size)
                    : results[0]);

                args.callback(returnPagedResrvations(resultSet, args.page, totalResultLength))

            })
        }
    },
    save: (args) => {

        const { data, callback } = args;

        let response = undefined;

        data.reservation_id = constructRefNo('reservation');
            
        let dataValidator = validate.validatePost(data, 'reservation');

        if (dataValidator.isValid) {

            data.id = uuid();

            connection.query('INSERT INTO `reservation` SET ?;', data , (err, results, fields) => {
                if (err) {

                    throw err;

                    response = {
                        action: 'Create reservation',
                        status: 'failed',
                        code: 400,
                        success: false,
                        err: errorHandler.throwSQLErr(err)
                    }

                    callback(response);

                } else {
                    if (results) {
                        data.link = `${urls.reservationUrl}/${data.id}`
                        response = {
                            action: 'Create reservation',
                            status: 'ok',
                            code: 200,
                            success: true,
                            message: 'reservation created',
                            results: {
                                reservation: data
                            }
                        }
                    }
                    callback(response);
                }

                });
            } else {
                response = {
                    action: 'create reservation',
                    status: 'failed',
                    code: 400,
                    success: false,
                    err: {
                        message: dataValidator.message,
                        resoulution: dataValidator.resolution,
                        err: dataValidator.err
                    }
                }

                callback(response);
        }
    }, update: (args) => {

        const { data, callback } = args;

        let response = undefined;

        let dataValidator = validate.validatePost(data, 'reservation');

        if (dataValidator.isValid) {

            data.id = uuid();

            connection.query('UPDATE reservation SET ?  WHERE ?;', data, (err, result, fields) => {
                if (err) {

                    response = {
                        action: 'Update reservation',
                        status: 'failed',
                        code: 400,
                        success: false,
                        err: errorHandler.throwSQLErr(err)
                    }

                    callback(response);

                } else {
                    if (results) {
                        data.link = `${urls.reservationUrl}/${data.id}`
                        response = {
                            action: 'Update reservation',
                            status: 'ok',
                            code: 200,
                            success: true,
                            message: 'reservation updated',
                            results: {
                                reservation: data
                            }
                        }
                    }
                    callback(response);
                }

                });
            } else {
                response = {
                    action: 'update reservation',
                    status: 'failed',
                    code: 400,
                    success: false,
                    err: {
                        message: dataValidator.message,
                        resoulution: dataValidator.resolution,
                        err: dataValidator.err
                    }
                }

                callback(response);
        }
    }
}