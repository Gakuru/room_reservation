const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const user = require('../modules/user');

router.get('/users', function (req, res) {
    user
        .find({}, function (err, users) {
            res.json(users);
        });
});

router.post('/authenticate', function (req, res) {
    // find the user
    user
        .getUser({
            email: req.body.email,
            password: req.body.password
        }, function (user, exists) {

            if (!exists) {
                res.json({success: false, message: 'Authentication failed. User not found.'});
            } else if (user) {

                // check if password matches
                if (user.pass != req.body.pass) {
                    res.json({success: false, message: 'Authentication failed. Wrong password.'});
                } else {
                    var token = jwt.sign({
                        user: {
                            id: user.id,
                            email: user.email,
                            lastName: user.lastName,
                            type: user.type
                        },
                        exp: Math.floor(Date.now() / 1000) + ((60 * 3) * 60)
                    }, //Expires in three hours
                            'secret');

                    // return the information including token as JSON
                    res.json({success: true, message: 'Enjoy your token!', user: user, token: token});
                }

            }

        });
});

module.exports = {
    router
};