const express = require('express');
const router = express.Router();

const common = require('../utils/common');

const room = require('../modules/room');


router.get('/', (req, res) => {
  res.send(common.AppName);
});

router.get('/rooms', (req, res) => {
  room.get({
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/rooms/page/:page', (req, res) => {
  room.get({
    page: parseInt(req.params.page),
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/rooms/:id', (req, res) => {
  room.get({
    id: req.params.id,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.post('/room', (req, res) => {

  room.save({
    data: {
      room_no: req.body.no,
      description: req.body.description,
      capacity: req.body.capacity
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

router.patch('/room', (req, res) => {
  room.update({
    data: [{
      no: req.body.title,
      description: req.body.description,
      capacity: req.body.capacity
    },{id: req.body.id,}],
    callback: (result) => {
      res.json(result);
    }
  });
});

module.exports = router;