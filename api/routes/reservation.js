const express = require('express');
const router = express.Router();

const common = require('../utils/common');

const reservation = require('../modules/reservation');


router.get('/', (req, res) => {
  res.send(common.AppName);
});

router.get('/reservations', (req, res) => {
  reservation.get({
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/reservations/page/:page', (req, res) => {
  reservation.get({
    page: parseInt(req.params.page),
    callback: (result) => {
      res.json(result);
    }
  });
});

router.get('/reservations/:id', (req, res) => {
  reservation.get({
    id: req.params.id,
    callback: (result) => {
      res.json(result);
    }
  });
});

router.post('/reservation', (req, res) => {
  reservation.save({
    data: {
      room_id: req.body.roomId,
      name: req.body.reservoir,
      mobile_no: req.body.reservoirMobileNo,
      amount: req.body.amountPaid,
      from_date: req.body.period.from,
      to_date: req.body.period.to
    },
    callback: (result) => {
      res.json(result);
    }
  });
});

router.patch('/reservation', (req, res) => {
  reservation.update({
    data: [{
      room_id: req.body.roomId,
      name: req.body.reservoir,
      mobile_no: req.body.reservoirMobileNo,
      amount: req.body.amountPaid,
      from_date: req.body.from,
      to_date: req.body.to
    },{id: req.body.id,}],
    callback: (result) => {
      res.json(result);
    }
  });
});

module.exports = router;