const urls = require('./urls');

const model = require('../models/model')

const dateUtils = require('./dateUtils');

module.exports = {
    transformRoom: (rooms) => {
        if (rooms) {
            const {room_no,description,capacity} = model.room;
            return rooms.map((room) => {
                return {
                    id: room.id.toString(),
                    [room_no.knownName]: room.room_no.toString(),
                    [description.knownName]: room.description.toString(),
                    [capacity.knownName]: room.capacity,                    
                    link: `${urls.roomUrl}/${room.id}`
                }
            })
        }

        return rooms;
    },
    transformReservations: (reservations) => {
        if (reservations) {
            const {room_id,reservation_id,name,mobile_no,amount,from_date,to_date} = model.reservation;
            return reservations.map((reservation) => {
                return {
                    id: reservation.id.toString(),
                    [room_id.knownName]: reservation.room_id.toString(),
                    [reservation_id.knownName]: reservation.reservation_id.toString(),
                    [name.knownName]: reservation.name.toString(),
                    [mobile_no.knownName]: reservation.mobile_no.toString(),
                    [amount.knownName]: reservation.amount,
                    status:reservation.amount > 0 ? 'paid':'unpaid',
                    period: {
                        [from_date.knownName]: reservation.from_date,
                        [to_date.knownName]: reservation.to_date,
                    },
                    link: `${urls.reservationUrl}/${reservation.id}`
                }
            })
        }

        return reservations;
    },
    transformCourseOnOffer: (courses) => {
        if (courses) {
            const {start_date,end_date,session_start_time,session_end_time,course_id,venue} = model.available_course;
            return courses.map((course) => {
                return {
                    id: course.offer_id
                        .toString(),
                    [course_id.knownName]: course.course_id,
                    title:course.title,
                    [start_date.knownName]: dateUtils
                        .format(course.start_date)
                        .toString(),
                    [end_date.knownName]: dateUtils
                        .format(course.end_date)
                        .toString(),
                    [session_start_time.knownName]: course
                        .session_start_time
                        .toString(),                    
                    [session_end_time.knownName]: course
                        .session_end_time
                        .toString(),
                    [venue.knownName]: course
                        .venue
                        .toString(),                    
                    link: `${urls.courseOfferUrl}/${course.offer_id}`
                }
            })
        }

        return courses;
    },
    transformProvidersNearBy: (providers) => {
        if (providers) {
            const {provider_name, lat, lng, distance} = model.providersNearBy;
            return providers.map((provider) => {
                return {
                    id: provider
                        .provider_id
                        .toString(),
                    [provider_name.knownName]: provider
                        .provider_name
                        .toString(),
                    [lat.knownName]: provider
                        .lat
                        .toString(),
                    [lng.knownName]: provider
                        .lng
                        .toString(),
                    [distance.knownName]: provider.distance
                }
            })
        }

        return providers;
    }
}