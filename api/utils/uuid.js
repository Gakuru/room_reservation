const uuidV1 = require('uuid/v1');

module.exports = {
    get: uuidV1()
}