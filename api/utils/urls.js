const urls = require('../../.constants/url');
const baseUrl = urls.baseUrl;

module.exports = {
    roomUrl: `${baseUrl}rooms`,
    reservationUrl: `${baseUrl}reservations`
}