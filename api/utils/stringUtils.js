module.exports = {
    replaceSpaceWithComma: (param) => {
        return param
            .split(' ')
            .join(',');
    }
}