const model = require('../models/model')

module.exports = {
    validatePost: (data, _model) => {

        let response = {
            isValid: true,
            message: undefined,
            resolution: undefined,
            err: []
        };

        if (_model) {

            const modelKeys = Object
                .keys(model[_model])
                .sort();

            const dataKeys = Object
                .keys(data)
                .sort();

            modelKeys.forEach(key => {
                if (model[_model][key].required) {
                    if (!data[key]) {
                        response
                            .err
                            .push(`'${model[_model][key].knownName}' is required but its value is missing`);
                        response.isValid = false;
                    }
                }
            });

            response.message = 'Reqired fields are missing values';
            response.resolution = 'Provide values for all required fields';

        } else {
            response.isValid = false;
            response.message = 'Invalid model';
            response.resolution = 'Provide the correct model name';
            response
                .err
                .push('Invalid model object provided');

        }

        return response;
    }
}