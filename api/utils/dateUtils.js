const moment = require('moment');

module.exports = {
    format: (date,format='DD-MM-YYYY') => {
        return moment(date).format(format);
    }
}