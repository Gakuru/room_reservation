
const mysql = require('mysql');

const conn = require('../../.constants/config')

const connection = mysql.createPool({
  connectionLimit: 15,
  host: conn.connectionString.host,
  user: conn.connectionString.user,
  password: conn.connectionString.password,
  database: conn.connectionString.database
});

module.exports = connection;