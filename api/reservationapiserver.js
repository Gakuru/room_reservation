const express = require('express');
const app = express();

const ports = [2040, 2041];
const bodyParser = require('body-parser');

const jwt = require('jsonwebtoken');

const auth = require('./auth/auth');

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,x-access-token');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

const room = require('./routes/room');
const reservation = require('./routes/reservation');
const user = require('./routes/user');

app.use((req, res, next) => {
    let _token = req.headers['x-api-key'];
    if (_token) {
        jwt
            .verify(_token, 'secret', function (err, decode) {
                if (err)
                    req.token = undefined;
                req.token = decode;
                next();
            });
    } else {
        req.token = undefined;
        next();
    }
});

app.use('/', user.router);
app.use('/', room);
app.use('/', reservation);

ports.forEach((port) => {
    app.listen(port, () => {
        console.log('api listening on port %s', port);
    });
});