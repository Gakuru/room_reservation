module.exports = {
    room: {
        id: {
            required: false,
            knownName: 'APP INJECT'
        },
        room_no: {
            required: true,
            knownName: 'no'
        },
        description: {
            required: true,
            knownName: 'description'
        },
        capacity: {
            required: false,
            knownName: 'capacity'
        }
    },
    reservation: {
        id: {
            required: false,
            knownName: 'APP INJECT'
        },
        room_id: {
            required: true,
            knownName: 'roomId'
        },
        reservation_id: {
            required: true,
            knownName: 'reservationId'
        },
        name: {
            required: true,
            knownName: 'reservoir'
        },
        mobile_no: {
            required: true,
            knownName: 'reservoirMobileNo'
        },
        amount: {
            required: true,
            knownName: 'amountPaid'
        },
        from_date: {
            required: true,
            knownName: 'from'
        },
        to_date: {
            required: true,
            knownName: 'to'
        }
    }
}