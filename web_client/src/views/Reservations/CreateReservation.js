import React, {Component} from 'react';
import {
  // Badge,
  Card,
  CardBody,
  CardHeader,
  CardFooter,
  Col,
  Row,
  Form,
  Label,
  FormGroup,
  InputGroup,
  Container,
  Button
} from 'reactstrap';

// import {AppSwitch} from '@coreui/react'

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {Link} from 'react-router-dom';

import moment from 'moment';
import DatePicker from 'react-datepicker';

import { requestSaveReservation } from '../../actions/reservations';

const object = {
  empty: (o) => {
    return Object.keys(o).length;
  }
}

class CreateReservation extends Component {

  constructor(props) {
    super(props);

    this.handleSave = this
      .handleSave
      .bind(this);

    this.state = {
      startDate: moment(),
      endDate: moment(),
      reservation:{
        reservationId: '',
        roomId:'',
        reservoir: '',
        reservoirMobileNo:'',
        reservationDate: '',
        period: {
          from: '',
          to: ''
        },
        amountPaid:0
      }
    };

    this.handleDateStart = this.handleDateStart.bind(this);

    this.handleDateEnd = this.handleDateEnd.bind(this);
    
    this.renderForm = this.renderForm.bind(this);

    this.handleInputChange = this.handleInputChange.bind(this);

  }

  componentDidMount() {
    if (object.empty(this.props.match.params) && !this.props.location.state) {
      // An edit with no data, get data from the db then set state
      console.info('async call');
    }
    else if(this.props.location.state){
      this.setState({ reservation: this.props.location.state.reservation });
    }  
  }

  handleInputChange = (event) => {
    let reservation = this.state.reservation;
    reservation[event.target.name] = event.target.value;
    this.setState({ reservation: reservation });
  }

  handleSave = () => {
    let reservation = this.state.reservation;
    reservation.period.from = moment(this.state.startDate).format('YYYY-MM-DD HH:mm:ss');
    reservation.period.to = moment(this.state.endDate).format('YYYY-MM-DD HH:mm:ss');

    this.setState({ reservation: reservation });
    
    this.props.requestSaveReservation(this.state.reservation);
    window.location = '/reservations';
  }

  handleDateStart = (date) => {
    this.setState({startDate: date});
  }

  handleDateEnd = (date) => {
    this.setState({endDate: date});
  }

  renderForm = () => {
    const { reservation } = this.state;

    return (
      <Form
        name="createReservation"
        onSubmit={(event) => {
        event.preventDefault()
      }}>

      <FormGroup>
        <Label htmlFor="reservationTo">Reservation to</Label>
        <div className="controls">
          <InputGroup className="input-prepend">
              <input
                className="form-control"
                name="reservoir"
                size="16"
                type="text"
                onChange={(event) => this.handleInputChange(event)}
                value={reservation ? reservation.reservoir : ''}
              />
          </InputGroup>
        </div>
      </FormGroup>

      <FormGroup>
        <Label htmlFor="reservoirMobileNo">Mobile No</Label>
        <div className="controls">
          <InputGroup className="input-prepend">
              <input
                className="form-control"
                name="reservoirMobileNo"
                size="16"
                type="text"
                onChange={(event) => this.handleInputChange(event)}
                value={reservation ? reservation.reservoirMobileNo : ''}
              />
          </InputGroup>
        </div>
      </FormGroup>

      <FormGroup>
        <Label htmlFor="roomId">Room</Label>
        <div className="controls">
          <InputGroup className="input-prepend">
              <input
                className="form-control"
                name="roomId"
                size="16"
                type="text"
                onChange={(event) => this.handleInputChange(event)}
                value={reservation ? reservation.roomId : ''}
              />
          </InputGroup>
        </div>
      </FormGroup>

      <FormGroup>
        <div
          className="controls"
          style={{
          marginLeft: -15
        }}>
          <InputGroup className="input-prepend">
            <Container>
              <Row>
                <Col>
                  <Label htmlFor="from">From:</Label>
                  <DatePicker
                    className="form-control"
                    dateFormat="LLL"
                    timeFormat="HH:mm"
                    selected={reservation.period.from?moment(reservation.period.from):this.state.startDate}
                    selectsStart
                    showTimeSelect
                    startDate={reservation.period.from?moment(reservation.period.from):this.state.startDate}
                    endDate={reservation.period.to?moment(reservation.period.to):this.state.endDate}
                    onChange={this.handleDateStart}/>
                </Col>
                <Col>
                  <Label htmlFor="to">To:</Label>
                  <DatePicker
                    className="form-control"
                    dateFormat="LLL"
                    timeFormat="HH:mm"
                    selected={reservation.period.to?moment(reservation.period.to):this.state.endDate}
                    selectsEnd
                    showTimeSelect
                    startDate={reservation.period.from?moment(reservation.period.from):this.state.startDate}
                    endDate={reservation.period.to?moment(reservation.period.to):this.state.endDate}
                    onChange={this.handleDateEnd}/>
                </Col>
              </Row>
            </Container>
          </InputGroup>
        </div>
      </FormGroup>

      <FormGroup>
        <Label htmlFor="amountPaid">Amount Paid</Label>
        <div className="controls">
          <InputGroup className="input-prepend">
              <input
                className="form-control"
                name="amountPaid"
                size="16"
                type="text"
                onChange={(event) => this.handleInputChange(event)}
                value={reservation ? reservation.amountPaid : ''}
              />
          </InputGroup>
        </div>
      </FormGroup>

      </Form>
    );
    
  }

  render() {
    return (
      <div className="animated fadeIn">

        <Card>
          <CardHeader>
            <Row className="align-items-center">
              <Col col="1" sm="2" md="2" className="">
                <Link to={`/reservations`} className="btn btn-outline-primary">
                  <i className="icon-arrow-left"></i>
                  &nbsp; Back
                </Link>
              </Col>
            </Row>
          </CardHeader>
        </Card>

        <Row>
          <Col xs="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-edit"></i>
                Create Reservation
              </CardHeader>
              <CardBody>
                
                {this.renderForm()}

              </CardBody>

              <CardFooter>
                <div className="ml-auto">
                  <Button type="submit" color="primary" onClick={this.handleSave}>
                    <i className="fa fa-save"></i>
                    &nbsp; Submit</Button>
                </div>
              </CardFooter>

            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (store) => {
  return {
      reservations: store.reservations.data
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({requestSaveReservation},dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CreateReservation);
