import React, {Component} from 'react';
import {Line} from 'react-chartjs-2';
import {
  
  ButtonDropdown,
  ButtonGroup,
  Card,
  CardBody,
  Col,
  Dropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Row,
} from 'reactstrap';
import {CustomTooltips} from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import {getStyle} from '@coreui/coreui/dist/js/coreui-utilities'

const brandPrimary = getStyle('--primary')
const brandInfo = getStyle('--info')

// Card Chart 1
const cardChartData1 = {
  labels: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July'
  ],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: brandPrimary,
      borderColor: 'rgba(255,255,255,.55)',
      data: [
        65,
        59,
        84,
        84,
        51,
        55,
        40
      ]
    }
  ]
};

const cardChartOpts1 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent'
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent'
        }
      }
    ],
    yAxes: [
      {
        display: false,
        ticks: {
          display: false,
          min: Math
            .min
            .apply(Math, cardChartData1.datasets[0].data) - 5,
          max: Math
            .max
            .apply(Math, cardChartData1.datasets[0].data) + 5
        }
      }
    ]
  },
  elements: {
    line: {
      borderWidth: 1
    },
    point: {
      radius: 4,
      hitRadius: 10,
      hoverRadius: 4
    }
  }
}

// Card Chart 2
const cardChartData2 = {
  labels: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July'
  ],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: brandInfo,
      borderColor: 'rgba(255,255,255,.55)',
      data: [
        1,
        18,
        9,
        17,
        34,
        22,
        11
      ]
    }
  ]
};

const cardChartOpts2 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent'
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent'
        }
      }
    ],
    yAxes: [
      {
        display: false,
        ticks: {
          display: false,
          min: Math
            .min
            .apply(Math, cardChartData2.datasets[0].data) - 5,
          max: Math
            .max
            .apply(Math, cardChartData2.datasets[0].data) + 5
        }
      }
    ]
  },
  elements: {
    line: {
      tension: 0.00001,
      borderWidth: 1
    },
    point: {
      radius: 4,
      hitRadius: 10,
      hoverRadius: 4
    }
  }
};

// Card Chart 3
const cardChartData3 = {
  labels: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July'
  ],
  datasets: [
    {
      label: 'My First dataset',
      backgroundColor: 'rgba(255,255,255,.2)',
      borderColor: 'rgba(255,255,255,.55)',
      data: [
        78,
        81,
        80,
        45,
        34,
        12,
        40
      ]
    }
  ]
};

const cardChartOpts3 = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips
  },
  maintainAspectRatio: false,
  legend: {
    display: false
  },
  scales: {
    xAxes: [
      {
        display: false
      }
    ],
    yAxes: [
      {
        display: false
      }
    ]
  },
  elements: {
    line: {
      borderWidth: 2
    },
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4
    }
  }
};

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.toggle = this
      .toggle
      .bind(this);
    this.onRadioBtnClick = this
      .onRadioBtnClick
      .bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2
    };
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({radioSelected: radioSelected});
  }

  render() {

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="6" lg="4">
            <Card className="text-white bg-info">
              <CardBody className="pb-0">
                <ButtonGroup className="float-right">
                  <ButtonDropdown
                    id='card1'
                    isOpen={this.state.card1}
                    toggle={() => {
                    this.setState({
                      card1: !this.state.card1
                    });
                  }}>
                    <DropdownToggle caret className="p-0" color="transparent">
                      <i className="icon-settings"></i>
                    </DropdownToggle>
                    <DropdownMenu right>
                      <DropdownItem>Action</DropdownItem>
                      <DropdownItem>Another action</DropdownItem>
                      <DropdownItem disabled>Disabled action</DropdownItem>
                      <DropdownItem>Something else here</DropdownItem>
                    </DropdownMenu>
                  </ButtonDropdown>
                </ButtonGroup>
                <div className="text-value">Reservations</div>
                <div className="row">
                  <div className="col-12"><hr/></div>
                  <div className="col-5">
                    Today
                  </div>
                  <div className="col-7">
                    <div className="float-right">
                      9.823
                    </div>
                  </div>
                  <div className="col-12"><hr/></div>
                  <div className="col-5">
                    This week
                  </div>
                  <div className="col-7">
                    <div className="float-right">
                      9.823
                    </div>
                  </div>
                  <div className="col-12"><hr/></div>
                  <div className="col-5">
                    This month
                  </div>
                  <div className="col-7">
                    <div className="float-right">
                      9.823
                    </div>
                  </div>
                </div>
              </CardBody>
              <div
                className="chart-wrapper mx-3"
                style={{
                height: '70px'
              }}>
                <Line data={cardChartData2} options={cardChartOpts2} height={70}/>
              </div>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="4">
            <Card className="text-white bg-warning">
              <CardBody className="pb-0">
                <ButtonGroup className="float-right">
                  <Dropdown
                    id='card3'
                    isOpen={this.state.card3}
                    toggle={() => {
                    this.setState({
                      card3: !this.state.card3
                    });
                  }}>
                    <DropdownToggle caret className="p-0" color="transparent">
                      <i className="icon-settings"></i>
                    </DropdownToggle>
                    <DropdownMenu right>
                      <DropdownItem>Action</DropdownItem>
                      <DropdownItem>Another action</DropdownItem>
                      <DropdownItem>Something else here</DropdownItem>
                    </DropdownMenu>
                  </Dropdown>
                </ButtonGroup>
                <div className="text-value">
                  Canceled Reservations
                </div>
                <Row>
                  <Col xs="12" sm="12" lg="12">
                    <hr/>
                  </Col>
                  <Col xs="12" sm="5" lg="5">
                    Today
                  </Col>
                  <Col xs="12" sm="7" lg="7">
                    <div className="float-right">
                      9.823
                    </div>
                  </Col>
                  <Col xs="12" sm="12" lg="12">
                    <hr/>
                  </Col>
                  <Col xs="12" sm="5" lg="5">
                    This week
                  </Col>
                  <Col xs="12" sm="7" lg="7">
                    <div className="float-right">
                      9.823
                    </div>
                  </Col>
                  <Col xs="12" sm="12" lg="12">
                    <hr/>
                  </Col>
                  <Col xs="12" sm="5" lg="5">
                    This month
                  </Col>
                  <Col xs="12" sm="7" lg="7">
                    <div className="float-right">
                      9.823
                    </div>
                  </Col>
                </Row>
              </CardBody>
              <div
                className="chart-wrapper"
                style={{
                height: '70px'
              }}>
                <Line data={cardChartData3} options={cardChartOpts3} height={70}/>
              </div>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="4">
            <Card className="text-white bg-primary">
              <CardBody className="pb-0">
                <ButtonGroup className="float-right">
                  <Dropdown
                    id='card2'
                    isOpen={this.state.card2}
                    toggle={() => {
                    this.setState({
                      card2: !this.state.card2
                    });
                  }}>
                    <DropdownToggle caret className="p-0" color="transparent">
                      <i className="icon-settings"></i>
                    </DropdownToggle>
                    <DropdownMenu right>
                      <DropdownItem>Action</DropdownItem>
                      <DropdownItem>Another action</DropdownItem>
                      <DropdownItem>Something else here</DropdownItem>
                    </DropdownMenu>
                  </Dropdown>
                </ButtonGroup>
                <div className="text-value">
                  Revenue
                </div>
                <Row>
                  <Col xs="12" sm="12" lg="12">
                    <hr/>
                  </Col>
                  <Col xs="12" sm="5" lg="5">
                    Today
                  </Col>
                  <Col xs="12" sm="7" lg="7">
                    <div className="float-right">
                      9.823
                    </div>
                  </Col>
                  <Col xs="12" sm="12" lg="12">
                    <hr/>
                  </Col>
                  <Col xs="12" sm="5" lg="5">
                    This week
                  </Col>
                  <Col xs="12" sm="7" lg="7">
                    <div className="float-right">
                      9.823
                    </div>
                  </Col>
                  <Col xs="12" sm="12" lg="12">
                    <hr/>
                  </Col>
                  <Col xs="12" sm="5" lg="5">
                    This month
                  </Col>
                  <Col xs="12" sm="7" lg="7">
                    <div className="float-right">
                      9.823
                    </div>
                  </Col>
                </Row>
              </CardBody>
              <div
                className="chart-wrapper mx-3"
                style={{
                height: '70px'
              }}>
                <Line data={cardChartData1} options={cardChartOpts1} height={70}/>
              </div>
            </Card>
          </Col>          
        </Row> 
      </div>
    );
  }
}

export default Dashboard;
