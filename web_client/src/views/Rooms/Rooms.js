import React, { Component } from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { requestGetRooms } from '../../actions/rooms';

import {
  Card,
  CardBody,
  CardHeader,
  Col,
  Row,
  Table
} from 'reactstrap';

import {Link} from 'react-router-dom';

const itemListNumber = (i, pager) => {
  return (pager?(((pager.currStartAt-1)*(pager.pageSize)+((++i)))):((++i)))
}

class Rooms extends Component {

  constructor(props) {
    super(props);

    this.state = {};

    this.renderRooms = this.renderRooms.bind(this);
    this.renderRows = this.renderRows.bind(this);
    this.getRooms = this.getRooms.bind(this);    
  }

  componentDidMount = () => {
    this.props.requestGetRooms();
  }

  getRooms(url) {
    this.props.requestGetRooms(url);
  }

  renderRooms = () => {
    if (this.props.rooms) {
      const { rooms, pager } = this.props.rooms;
      return (
        <Table hover striped responsive>
          <thead>
            <tr>
              <th>
                <div>#</div>
              </th>
              <th>
                <div>Room No</div>
              </th>
              <th>
                <div>Capacity</div>
              </th>
              <th>
                <div>Description</div>
              </th>
              <th>
                <div className="float-right">Actions</div>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.renderRows(rooms,pager)}
          </tbody>
        </Table>
      )
    }
  }

  renderRows = (rooms, pager) => {
    if (rooms) {
      return rooms.map((room, i) => {
        return (
          <tr key={room.id}>
            <td>
              <div>{itemListNumber(i,pager)}</div>
            </td>
            <td>
              <div>{room.no}</div>
            </td>
            <td>
              <div>{room.capacity}</div>
            </td>
            <td>
              <div>{room.description}</div>
            </td>            
            <td>
              <div className="float-right">
                <Link
                  to={{
                  pathname: `/rooms/${room
                    .no
                    .toLowerCase()}/edit`,
                  state: {
                    room: room
                  }
                }}>
                  <i className="icon-pencil"></i>
                </Link>
                &emsp;
                <Link
                  to={`/rooms/${room
                  .no
                  .toLowerCase()}/remove`}
                  className="text-danger">
                  <i className="icon-trash"></i>
                </Link>
              </div>
            </td>
          </tr>
        );
      })
    }
  }

  render() {
    return (
      <div className="animated fadeIn">

        <Card>
          <CardHeader>
            <Row className="align-items-center">
              <Col col="1" sm="2" md="2" className="">
                <Link to={`/rooms/create`} className="btn btn-outline-primary">
                  <i className="icon-plus"></i>
                  &nbsp; New Room
                </Link>
              </Col>
            </Row>
          </CardHeader>
        </Card>

        <Row>
          <Col xs="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i>
                Rooms
              </CardHeader>
              <CardBody
                style={{
                height: '45vh',
                overflow: 'auto'
              }}>
                {this.renderRooms()}
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (store) => {
  return {
      rooms: store.rooms
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({requestGetRooms},dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Rooms);
