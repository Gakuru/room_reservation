import React, { Component } from 'react';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  // Badge,
  Card,
  CardBody,
  CardHeader,
  CardFooter,
  Col,
  Row,
  Form,
  Label,
  FormGroup,
  InputGroup,
  // Table,
  Button
} from 'reactstrap';

import { Link } from 'react-router-dom';

import { requestSaveRoom } from '../../actions/rooms';

const object = {
  empty: (o) => {
    return Object.keys(o).length;
  }
}

class CreateRooms extends Component {

  constructor(props) {
    super(props);

    this.state = {
      room:{
        no: '',
        capacity: '',
        description: ''
      }
    }

    this.renderForm = this.renderForm.bind(this);

    this.handleSave = this.handleSave.bind(this);
    
    this.handleInputChange = this.handleInputChange.bind(this);

  }

  componentDidMount() {
    if (object.empty(this.props.match.params) && !this.props.location.state) {
      // An edit with no data, get data from the db then set state
      console.info('async call');
    }
    else if(this.props.location.state){
      this.setState({ room: this.props.location.state.room });
    }
  
  }

  handleInputChange = (event) => {
    let room = this.state.room;
    room[event.target.name] = event.target.value;
    this.setState({ room: room });
  }

  handleSave = () => {    
    this.props.requestSaveRoom(this.state.room);
    window.location = '/rooms';
  }

  renderForm = () => {
    const { room } = this.state;
      return (
        <Form
            name="createRoom"
            onSubmit={(event) => {
            event.preventDefault()
          }}>

            <FormGroup>
              <Label htmlFor="no">Room No</Label>
              <div className="controls">
                <InputGroup className="input-prepend">
                  <input
                    name="no"
                    size="16"
                    className="form-control"
                    type="text"
                    onChange={(event)=>this.handleInputChange(event)}
                    value={room?room.no:''}
                  />
                </InputGroup>
              </div>
            </FormGroup>

            <FormGroup>
              <Label htmlFor="description">Description</Label>
              <div className="controls">
                <InputGroup className="input-prepend">
                  <input
                    name="description"
                    size="16"
                    type="text"
                    className="form-control"
                    onChange={(event)=>this.handleInputChange(event)}
                    value={room?room.description:''}
                  />
                </InputGroup>
              </div>
            </FormGroup>

            <FormGroup>
              <Label htmlFor="capacity">Capacity</Label>
              <div className="controls">
                <InputGroup className="input-prepend">
                  <input
                    name="capacity"
                    size="16"
                    type="text"
                    className="form-control"
                    onChange={(event)=>this.handleInputChange(event)}
                    value={room?room.capacity:''}
                  />
                </InputGroup>
              </div>
          </FormGroup>
          
        </Form>
      )
  }

  render() {
    return (
      <div className="animated fadeIn">

        <Card>
          <CardHeader>
            <Row className="align-items-center">
              <Col col="1" sm="2" md="2" className="">
                <Link to={`/rooms`} className="btn btn-outline-primary">
                  <i className="icon-arrow-left"></i>
                  &nbsp; Back
                </Link>
              </Col>
            </Row>
          </CardHeader>
        </Card>

        <Row>
          <Col xs="12" md="12" lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-edit"></i>
                Create Room
              </CardHeader>
              <CardBody>

                {this.renderForm()}

              </CardBody>

              <CardFooter>
                <div className="ml-auto">
                  <Button type="submit" color="primary" onClick={this.handleSave}>
                    <i className="fa fa-save"></i>
                    &nbsp; Submit</Button>
                </div>
              </CardFooter>

            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (store) => {
  return {
      rooms: store.rooms.data
  }
}

const mapDispatchToProps = (dispatch) => bindActionCreators({requestSaveRoom},dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(CreateRooms);
