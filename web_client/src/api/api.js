// import axios from 'axios';

import urls from 'app-urls/url';
const baseUrl = urls.baseUrl;

export const fetchRooms = async (url) => {
    // return axios.get(`http://localhost:8084/courses`);
    try {        
        const response = await fetch(url.url?url.url:`${baseUrl}rooms`);
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

export const saveRoom = async (room) => {
    try {
        const response = await fetch(`${baseUrl}room`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(room)
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

export const updateRoom = async (room) => {
    try {
        const response = await fetch(`${baseUrl}room`, {
            method: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(room)
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}




export const fetchReservations = async (url) => {
    // return axios.get(`http://localhost:8084/courses`);
    try {        
        const response = await fetch(url.url?url.url:`${baseUrl}reservations`);
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

export const saveReservation = async (room) => {
    try {
        const response = await fetch(`${baseUrl}reservation`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(room)
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}

export const updateReservation = async (room) => {
    try {
        const response = await fetch(`${baseUrl}reservation`, {
            method: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(room)
        });
        return await response.json();
    } catch (e) {
        console.log(e);
    }
}