export default {
  items : [
    {
      title: true,
      name: 'Accomodation',
      wrapper: {
        element: '',
        attributes: {}
      },
      class: ''
    }, {
      name: 'Rooms',
      url: '/rooms',
      icon: 'icon-home'
    }, {
      title: true,
      name: 'Reservations',
      wrapper: {
        element: '',
        attributes: {}
      },
      class: ''
    }, {
      name: 'Reservations',
      url: '/reservations',
      icon: 'icon-clock'
    }
  ]
};
