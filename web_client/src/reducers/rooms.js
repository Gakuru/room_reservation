import { RECEIVE_GET_ROOM } from "../actions/rooms";


export const GetRooms = (state = [], { type,rooms }) => {
    switch (type) {
        case RECEIVE_GET_ROOM: {
            return rooms;
        }
        default: {
            return state;
        }
    }
}