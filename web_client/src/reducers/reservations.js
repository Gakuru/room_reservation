import { RECEIVE_GET_RESERVATIONS } from "../actions/reservations";


export const GetReservations = (state = [], { type,reservations }) => {
    switch (type) {
        case RECEIVE_GET_RESERVATIONS: {
            return reservations;
        }
        default: {
            return state;
        }
    }
}