import { combineReducers } from 'redux';

import {GetRooms as rooms} from './rooms';
import {GetReservations as reservations} from './reservations';

export default combineReducers({
    rooms,
    reservations
});