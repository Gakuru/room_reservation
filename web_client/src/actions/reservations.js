export const REQUEST_GET_RESERVATIONS = 'REQUEST_GET_RESERVATIONS';
export const RECEIVE_GET_RESERVATIONS = 'RECEIVE_GET_RESERVATIONS';

export const REQUEST_SAVE_RESERVATION = 'REQUEST_SAVE_RESERVATION';
export const SAVE_RESERVATION_SUCCESS = 'SAVE_RESERVATION_SUCCESS';


export const requestGetReservations = url => ({ type: REQUEST_GET_RESERVATIONS,url });
export const receiveGetReservations = reservations => ({ type: RECEIVE_GET_RESERVATIONS, reservations });

export const requestSaveReservation = reservation => ({ type: REQUEST_SAVE_RESERVATION,reservation });
export const saveReservationSuccess =  () => ({ type: SAVE_RESERVATION_SUCCESS });