export const REQUEST_GET_ROOM = 'REQUEST_GET_ROOM';
export const RECEIVE_GET_ROOM = 'RECEIVE_GET_ROOM';

export const REQUEST_SAVE_ROOM = 'REQUEST_SAVE_ROOM';
export const SAVE_ROOM_SUCCESS = 'SAVE_ROOM_SUCCESS';


export const requestGetRooms = url => ({ type: REQUEST_GET_ROOM,url });
export const receiveGetRooms = rooms => ({ type: RECEIVE_GET_ROOM, rooms });

export const requestSaveRoom = room => ({ type: REQUEST_SAVE_ROOM,room });
export const saveRoomSuccess =  () => ({ type: SAVE_ROOM_SUCCESS });