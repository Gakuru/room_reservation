import { all } from 'redux-saga/effects';

import {roomSaga,saveRoomSaga} from './rooms';
import {reservationSaga,saveReservationSaga} from './reservations';

export default function* rootSaga() {
    yield all([
        roomSaga(),
        saveRoomSaga(),
        reservationSaga(),
        saveReservationSaga()
    ])
  }