import { put, call, takeLatest } from 'redux-saga/effects';

import { REQUEST_GET_ROOM, receiveGetRooms, saveRoomSuccess, REQUEST_SAVE_ROOM } from '../actions/rooms';

import { fetchRooms,saveRoom as _saveRoom, updateRoom as _updateRoom } from '../api/api';

function* getRoom(url) {
    try {
        //API Call
        const data = yield call(fetchRooms, url);
        yield put(receiveGetRooms(data));
    } catch (e) {
        // Catch error
        // yield put(receiveGetCourses(courses));
        console.log(e);
    }
}

function* saveRoom({room}) {
    try {
        //API Call
        let data = undefined
        if (room.id)
            data = yield call(_updateRoom, room);
        
        data = yield call(_saveRoom, room);

        if(data.success)
            yield put(saveRoomSuccess(data));
    } catch (e) {
        // Catch error
        // yield put(receiveGetCourses(courses));
        console.log(e);
    }
}

export function* roomSaga() {
    yield takeLatest(REQUEST_GET_ROOM,getRoom);
}

export function* saveRoomSaga() {
    yield takeLatest(REQUEST_SAVE_ROOM, saveRoom);
}