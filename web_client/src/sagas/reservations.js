import { put, call, takeLatest } from 'redux-saga/effects';

import { REQUEST_GET_RESERVATIONS, receiveGetReservations, saveReservationSuccess, REQUEST_SAVE_RESERVATION } from '../actions/reservations';

import { fetchReservations,saveReservation as _saveReservation, updateReservation as _updateReservation } from '../api/api';

function* getReservation(url) {
    try {
        //API Call
        const data = yield call(fetchReservations, url);
        yield put(receiveGetReservations(data));
    } catch (e) {
        // Catch error
        // yield put(receiveGetCourses(courses));
        console.log(e);
    }
}

function* saveReservation({reservation}) {
    try {
        //API Call
        let data = undefined
        if (reservation.id)
            data = yield call(_updateReservation, reservation);
        
        data = yield call(_saveReservation, reservation);

        if(data.success)
            yield put(saveReservationSuccess(data));
    } catch (e) {
        // Catch error
        // yield put(receiveGetCourses(courses));
        console.log(e);
    }
}

export function* reservationSaga() {
    yield takeLatest(REQUEST_GET_RESERVATIONS,getReservation);
}

export function* saveReservationSaga() {
    yield takeLatest(REQUEST_SAVE_RESERVATION, saveReservation);
}