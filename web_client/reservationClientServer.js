var express = require('express');

var server = express();

server.use(express.static(__dirname+'/build'));

server.get('/*', function(req, res){
  res.sendFile(__dirname + '/build/index.html');
});

var port = 91;
server.listen(port, function() {
  console.log('client running on port ' + port);
});
