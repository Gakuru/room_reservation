const inDevMode = false;
const remoteDB = false;
const port = 8085

module.exports = {

    reportsUrl: ( function () {
        return 'http://207.154.207.193:5488';
    })(),

    baseUrl: ( function () {
        if (inDevMode) 
            return 'http://localhost:'+port+'/';
        else 
            return 'http://188.166.37.51:'+port+'/';
        }
    )(),

    dbHost: ( function () {
        if (inDevMode && !remoteDB)
            return 'localhost';
        else 
            return '188.166.37.51';
        }
    )()

};