
const url = require('./url');

module.exports = {
    baseUrl: url.baseUrl,
    connectionString:{
        host: url.dbHost,
        user: 'root',
        password: 'none',
        database: 'reservation'
    }
};
